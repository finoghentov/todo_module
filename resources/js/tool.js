Nova.booting((Vue, router, store) => {
  router.addRoutes([
    {
      name: 'todo-module',
      path: '/todo-module',
      component: require('./components/index/App'),
      props: true
    },
    {
      name: 'add-table',
      path: '/todo-module/add-table',
      component: require('./components/add_table/App'),
      props: true
    },
    {
     name: 'add-card',
     path: '/nova-vendor/todo-module/add-card'
    }
  ])
})

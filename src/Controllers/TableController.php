<?php


namespace Finoghentov\TodoModule\Controllers;

use Finoghentov\TodoModule\Models\Table;
use Finoghentov\TodoModule\Requests\Table\AddTableRequest;
use Finoghentov\TodoModule\Requests\Table\ChangeTablePositionRequest;
use Finoghentov\TodoModule\Requests\Table\DeleteTableRequest;

class TableController extends ApiController
{
    /**
     * @return Table json
     */
    public function getTables(){
        return parent::api_getTables();
    }

    /**
     * @param AddTableRequest $request
     * @return string json
     */
    public function addTable(AddTableRequest $request){
        return parent::api_addTable($request);
    }

    /**
     * @param DeleteTableRequest $request
     * @return string json
     */
    public function deleteTable(DeleteTableRequest $request){
        return parent::api_deleteTable($request);
    }

    /**
     * @param DeleteTableRequest $request
     * @return string json
     */
    public function changeTablePosition(ChangeTablePositionRequest $request){
        return parent::api_changeTablePosition($request);
    }

}

<?php


namespace Finoghentov\TodoModule\Controllers;


use Finoghentov\TodoModule\Models\Card;
use Finoghentov\TodoModule\Requests\Cards\AddCardRequest;
use Finoghentov\TodoModule\Requests\Cards\ChangeCardPositionRequest;
use Finoghentov\TodoModule\Requests\Cards\DeleteCardRequest;
use Finoghentov\TodoModule\Requests\Cards\GetCardsRequest;
use Finoghentov\TodoModule\Requests\Cards\UpdateCardRequest;


class CardController extends ApiController
{
    /**
     * @param GetCardsRequest $request
     * @return Card json
     */
    public function getCards(GetCardsRequest $request){
        return parent::api_getCards($request);
    }

    /**
     * @param AddCardRequest $request
     * @return Card json
     */
    public function addCard(AddCardRequest $request){
        return parent::api_addCard($request);
    }

    /**
     * @param UpdateCardRequest $request
     * @return Card json
     */
    public function updateCard(UpdateCardRequest $request){
        return parent::api_updateCard($request);
    }

    /**
     * @param DeleteCardRequest $request
     * @return string
     */
    public function deleteCard(DeleteCardRequest $request){
        return parent::api_deleteCard($request);
    }

    /**
     * @param ChangeCardPositionRequest $request
     * @return string
     */
    public function changeCardPosition(ChangeCardPositionRequest $request){
        return parent::api_changeCardPosition($request);
    }

}

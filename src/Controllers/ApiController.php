<?php


namespace Finoghentov\TodoModule\Controllers;

use App\Http\Controllers\Controller;
use Finoghentov\TodoModule\Models\Card;
use Finoghentov\TodoModule\Models\Table;
use Finoghentov\TodoModule\Models\Task;
use Finoghentov\TodoModule\Requests\ApiRequest;

class ApiController extends Controller
{
    /**
     * Return all existing tables
     * @return Table json
     */
    public function api_getTables(){
        $tables = Table::select('id', 'name')->order()->get();

        return response()->json($tables);
    }

    /**
     * Check Model boot
     * @param ApiRequest $request
     * @return string json
     */
    public function api_addTable(ApiRequest $request){
        try{
            Table::create($request->all());
        }catch(\Exception $e){
            return response()->json('Error while creating table', 422);
        }
        return response()->json('success');
    }

    /**
     * @param ApiRequest $request
     * @return string json
     */
    public function api_deleteTable(ApiRequest $request){

        $table = Table::findOrFail($request->id);

        try{
            $table->delete();
        }catch (\Exception $e){
            return response()->json('Error with deleting table', 422);
        }

        return response()->json('success', 200);
    }

    /**
     * @param ApiRequest $request
     * @return string json
     */
    public function api_changeTablePosition(ApiRequest $request){
        try{
            Table::sortTables($request->id, $request->order);
        }catch(\Exception $e){
            return response()->json('Error while sorting table', 422);
        }

        return response()->json('success', 200);
    }

    /**
     * @param ApiRequest $request
     * @return Card json
     */
    public function api_getCards(ApiRequest $request){
        $cards = Card::with('tasks')->whereTable($request->table_id)->order()->get();

        return response()->json($cards);
    }

    /**
     * Check Model boot
     * @param ApiRequest $request
     * @return Card json
     */
    public function api_addCard(ApiRequest $request){
        try{
            $card = Card::create($request->all());
        }catch(\Exception $e){
            return response()->json('Error while creating new card', 422);
        }

        return response()->json($card);
    }

    /**
     * @param ApiRequest $request
     * @return Card json
     */
    public function api_updateCard(ApiRequest $request){

        try{
            $card = Card::find($request->id);
            $card->update($request->all());
        }catch(\Exception $e){
            return response()->json('Error while updating card', 422);
        }

        return response()->json($card);
    }

    /**
     * @param ApiRequest $request
     * @return string json
     */
    public function api_changeCardPosition(ApiRequest $request){
        try{
            Card::sortTableCards($request->id, $request->table_id, $request->order);
        }catch(\Exception $e){
            return response()->json('Error while sorting Cards', 422);
        }

        return response()->json('success');

    }

    /**
     * @param ApiRequest $request
     * @return string json
     */
    public function api_deleteCard(ApiRequest $request){

        $card = Card::findOrFail($request->id);

        try{
            $card->delete();
        }catch(\Exception $e){
            return response()->json('Error with deleting', 422);
        }

        return response()->json('success');
    }

    /**
     * @param ApiRequest $request
     * @return Task json
     */
    public function api_addTask(ApiRequest $request){
        try{
            $task = Task::create($request->all());
        }catch(\Exception $e){
            return response()->json('Failed Create Task', 422);
        }

        return response()->json($task);
    }

    /**
     * @param ApiRequest $request
     * @return string json
     */
    public function api_deleteTask(ApiRequest $request){

        try{
            $task = Task::find($request->id);
            $task->delete();
        }catch(\Exception $e){
            return response()->json('Failed Delete Task', 422);
        }

        return response()->json('success');
    }

    /**
     * @param ApiRequest $request
     * @return string json
     */
    public function api_changeTaskPosition(ApiRequest $request){

        $task = Task::findOrFail($request->id);

        $data = [
            'prev_card_id' => $request->prev_card_id,
            'card_id' => $request->card_id,
            'old_index' => $request->old_index,
            'order' => $request->order
        ];

        try{
            Task::sortCurrentCardTasks($data, $task);
        }catch(\Exception $e){
            return response()->json('Failed Sort Current Card', 422);
        }

        $task->card_id = $request->card_id;

        try{
            $task->save();
        }catch(\Exception $e){
            return response()->json('Failed to Save Task', 422);
        }

        if($request->prev_card_id != $request->card_id){
            try{
                Task::sortPrevCardTask($request->prev_card_id);
            }catch(\Exception $e){
                return response()->json('Failed Sort Prev Card', 422);
            }
        }

        return response()->json('success');
    }
}

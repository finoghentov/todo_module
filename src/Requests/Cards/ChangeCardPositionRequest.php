<?php

namespace Finoghentov\TodoModule\Requests\Cards;

use Finoghentov\TodoModule\Requests\ApiRequest;

class ChangeCardPositionRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'table_id' => 'required|integer',
            'id' => 'required|integer',
            'order' => 'required|integer'
        ];
    }
}

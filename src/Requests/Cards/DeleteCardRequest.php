<?php

namespace Finoghentov\TodoModule\Requests\Cards;

use Finoghentov\TodoModule\Requests\ApiRequest;

class DeleteCardRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer'
        ];
    }
}

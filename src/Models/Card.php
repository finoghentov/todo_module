<?php


namespace Finoghentov\TodoModule\Models;


use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table = 'todo_cards';

    protected $guarded = [];

    public function tasks(){
        return $this->hasMany(Task::class)->order();
    }

    public function scopeOrder($query){
        return $query->orderBy('order', 'asc');
    }

    public function scopeWhereTable($query, $table_id){
        return $query->where('table_id', $table_id);
    }

    /**
     * Getting order sorting for new element
     * @param $table_id int
     * @return int
     */
    private static function getNewOrder($table_id){
        return self::where('table_id', $table_id)->count();
    }

    /**
     * Sorting cards in table after moving one of them
     * @param $card_id int
     * @param $table_id int
     * @param $order int
     * @return void
     */
    public static function sortTableCards($card_id, $table_id, $order){
        $card = self::find($card_id);
        $cards = self::where('id', '!=', $card_id)->where('table_id', $table_id)->orderBy('order', 'asc')->get();
        $cards->splice($order, 0,  [$card]);
        $i = 1;
        foreach($cards as $card){
            $card->order = $i;
            $card->save();
            $i++;
        }
    }

    /**
     * Before deleting Model , delete relations
     */
    public static function boot() {
        parent::boot();
        /**
         * Setting order sort after creating
         */
        self::created(function($card){
            $card->order = self::getNewOrder($card->table_id);
            $card->save();
        });

        /**
         * Before deleting Model , delete relations
         */
        self::deleting(function($card) {
            $card->tasks()->each(function($task) {
                $task->delete();
             });
        });
    }
}

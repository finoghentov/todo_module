<?php

namespace Finoghentov\TodoModule;

use Laravel\Nova\Nova;
use Laravel\Nova\Tool;

class TodoModule extends Tool
{
    protected $title;

    /**
     * First attribute for changing sidebar title.
     * @var $component
     * @var $title string
     * @return void
     */
    public function __construct($title = 'Tasks', $component = null)
    {
        $this->title = $title;
        parent::__construct($component);
    }

    /**
     * Perform any tasks that need to happen when the tool is booted.
     *
     * @return void
     */
    public function boot()
    {
        Nova::script('todo-module', __DIR__.'/../dist/js/tool.js');
        Nova::style('todo-module', __DIR__.'/../dist/css/tool.css');
    }


    /**
     * Build the view that renders the navigation links for the tool.
     *
     * @return \Illuminate\View\View
     */
    public function renderNavigation()
    {
        $title = $this->title;
        return view('todo-module::navigation', compact('title'));
    }
}

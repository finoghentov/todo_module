<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

Route::get('/get-tables', 'TableController@getTables');

Route::post('/add-table', 'TableController@addTable');

Route::post('/delete-table', 'TableController@deleteTable');

Route::post('/change-table-position', 'TableController@changeTablePosition');

Route::post('/get-cards', 'CardController@getCards');

Route::post('/add-card', 'CardController@addCard');

Route::post('/update-card', 'CardController@updateCard');

Route::post('/delete-card', 'CardController@deleteCard');

Route::post('/change-card-position', 'CardController@changeCardPosition');

Route::post('/add-task', 'TaskController@addTask');

Route::post('/delete-task', 'TaskController@deleteTask');

//Route::post('/change-status-task', 'TodoController@changeStatusText');

Route::post('/change-task-position', 'TaskController@changeTaskPosition');
